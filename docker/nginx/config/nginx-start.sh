#!/bin/bash
cp /opt/etc/nginx.conf /etc/nginx/nginx.conf
sed -i "s/%fpm-ip%/$FPM_PORT_9000_TCP_ADDR/" /etc/nginx/nginx.conf
chgrp -R www-data /data/www/storage
chmod -R g+w /data/www/storage
chmod -R 775 /data/www/storage

exec /usr/sbin/nginx
