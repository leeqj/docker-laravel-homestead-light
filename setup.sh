#!/bin/bash

ENV="$1";
PRJ="$2";
DIR="data/www";
CUR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd );
COP="docker-compose.yml";
NAME="homestead.com";
source ~/.bash_profile;

runDockerComposeSetup()
{
	if [ "$ENV" = "" ]
	then
		echo "Please provide the environment: local, dev, stage, prod, jenkins";
		exit 1;
	fi

	if [[ "$PRJ" = "" && "$ENV" != "local" ]]
	then
		echo "Please provide the environment project name: (ex: MyProjDev)";
		exit 1;
	fi

	if [ "$ENV" != "" ]
	then
		copyDockerComposeFile;
		startServices;
		listServices;
		copyFiles;
	fi
}

listServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" ps;
	else
		docker-compose ps;
	fi
}

startServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" up -d;
	else
		docker-compose up -d;
	fi
}

stopServices()
{
	if [ -e "$COP" ]
	then
		if [ "$PRJ" != "" ]
		then
			docker-compose -p "$PRJ" stop;
		else
			docker-compose stop;
		fi
	fi
}

copyDockerComposeFile()
{
	cp "env/$ENV/docker-compose.yml" "$COP";
}

runComposer()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" run composer self-update;
		docker-compose -p "$PRJ" run composer update;
	else
		docker-compose run composer self-update;
		docker-compose run composer update;
	fi
}

runArtisanMethods()
{
	if [ -d "$DIR/vendor" ]
	then
		if [[ "$PRJ" != "" && "$ENV" != "jenkins" ]]
		then
			docker-compose -p "$PRJ" run artisan migrate;
		else
			docker-compose run artisan migrate;
		fi
	fi
}

createFolders()
{
	mkdir -p data/logs;
	mkdir -p data/www/storage;
	mkdir -p data/www/storage/app;
	mkdir -p data/www/storage/logs;
	mkdir -p data/www/storage/framework;
	mkdir -p data/www/storage/framework/cache;
	mkdir -p data/www/storage/framework/sessions;
	mkdir -p data/www/storage/framework/views;
}

runLocalNetworkSetup()
{
	echo Starting boot2docker if not already started;
	boot2docker status || boot2docker up;

	export the_ip=$(boot2docker ip 2>&1 | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	export old_route_ip=$(netstat -r | grep 172.17  | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	echo Found boot2docker ip $the_ip;

	if [ "$the_ip" == "" ]; then
		echo Could not find the ip of boot2docker.. =/;
		exit;
	fi

	echo Seting up routes. Enter sudo password;
	if [ "$old_route_ip" != "" ]; then
		sudo route -n delete -net 172.17.0.0 $old_route_ip;
	fi
	sudo route -n add -net 172.17.0.0 $the_ip;

	echo Setting up hosts;
	cp /etc/hosts hosts;
	grep -v '# boot2dockerscripthomestead' hosts > hosts_temp;
	mv hosts_temp hosts;
	for containerid in $(docker ps -q); do
		export domain=$(docker inspect $containerid | grep '"Domainname":' | cut -f2 -d":" | cut -f2 -d'"');
		export host=$(docker inspect $containerid | grep '"Hostname":' | cut -f2 -d":" | cut -f2 -d'"');
		export ip=$(docker inspect $containerid | grep 'IPAdd' | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
		
		if [[ "$domain" != "" && $domain == *"$NAME"* && "$ENV" != "jenkins" ]]; then

			env=`echo $domain| cut -d '.' -f 1`;
			cp "env/$env/$env" "$DIR/$domain.config";
			echo Storing values $ip $host.$domain;
			echo $ip $host $domain \# boot2dockerscripthomestead >> hosts;
		fi
	done
	sudo mv hosts /etc/hosts;
}

copyFiles()
{
	cp "env/$ENV/$ENV" "$DIR/.env";
}

createFolders;
runDockerComposeSetup;

if [ ! -d "$DIR/vendor" ]
then
	runComposer;
	startServices;
	listServices;
	copyFiles;
fi

runLocalNetworkSetup;
runArtisanMethods;
